See https://www.resultsbasedteddybear.io/posts/python-interfacing-with-ibkr/ for a full walk through of the code.

This repo demonstrates connecting to the IBKR workstation or gateway and subscribing to market data/real time bars.