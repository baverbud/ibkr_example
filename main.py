from ib_app import IbApp
from ibapi.client import *
from ibapi.wrapper import *

import argparse
import json

def parse_args():
    parser = argparse.ArgumentParser(
                    prog='ibkr_example',
                    description='Connects to IB Gateway and logs market data specified by a config file.')
    
    parser.add_argument('-c', '--config', required=True)

    return parser.parse_args()

def load_config(app, config):
    ticker_id = 1
    for contract in config['contracts']:
        c = Contract()
        c.symbol = contract["Symbol"]
        c.secType = contract["Type"]
        c.exchange = contract["Exchange"]
        c.currency = contract["Currency"]
        if "PrimaryExchange" in contract:
            c.primaryExchange = contract["PrimaryExchange"]
        
        app.reqContractDetails(ticker_id, c)
        app.reqMktData(ticker_id, c, "", False, False, [])
        app.reqRealTimeBars(ticker_id, c, 5, "MIDPOINT", True, []);
        ticker_id += 1


def main():
    args = parse_args()

    app = IbApp()

    app.connect("localhost", 7497, 1000)

    with open(args.config, "r") as fd:
        config = json.load(fd)

    load_config(app, config)

    app.start_run_thread()

    print("Press any key to quit")
    input()

    app.stop_run_thread()


if __name__ == "__main__":
    main()