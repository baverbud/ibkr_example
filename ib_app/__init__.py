from ibapi.client import *
from ibapi.wrapper import *
from ibapi.ticktype import TickTypeEnum

from time import time_ns

from threading import Thread


class IbApp(EClient, EWrapper):
    def __init__(self):
        EClient.__init__(self, self)
        self.init_time = time_ns()
        self.contracts_by_id = {}
        self.thread = None
    
    def start_run_thread(self):
        if self.thread is not None:
            raise Exception("Thread already started")
        
        self.thread = Thread(target=self.run)
        self.thread.start()

    def stop_run_thread(self):
        self.disconnect()
        self.thread.join()
        self.thread = None

    def contractDetails(self, reqId, contractDetails):
        self.contracts_by_id[reqId] = contractDetails
    
    def contractDetailsEnd(self, reqId):
        pass

    def tickPrice(self, reqId, tickType, price, attrib):
        if reqId in self.contracts_by_id:
            timestamp = time_ns() - self.init_time
            contract = self.contracts_by_id[reqId].contract
            print(f"{timestamp},TICK_PRICE,{contract.symbol},{contract.secType},{contract.currency},{contract.exchange},{TickTypeEnum.toStr(tickType)},{price}")

    def tickSize(self, reqId, tickType, size):
        if reqId in self.contracts_by_id:
            timestamp = time_ns() - self.init_time
            contract = self.contracts_by_id[reqId].contract

            print(f"{timestamp},TICK_SIZE,{contract.symbol},{contract.secType},{contract.currency},{contract.exchange},{TickTypeEnum.toStr(tickType)},{int(size)}")
    
    def realtimeBar(self, reqId: TickerId, time:int, open_: float, high: float, low: float, close: float,
                             volume: Decimal, wap: Decimal, count: int):
        if reqId in self.contracts_by_id:
            timestamp = time_ns() - self.init_time
            contract = self.contracts_by_id[reqId].contract
            
            print(f"{timestamp},BAR,{contract.symbol},{open_},{high},{low},{close},{volume},{wap},{count}")
